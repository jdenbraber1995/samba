package mpc;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import examples.hz.com.samba.Mp3File;

/**
 * Handles playlist manipulation on the MPD server. Allows songs to be enqued on
 * the MPD server's playlist.
 * 
 * @author thelollies
 */

public class PlaylistThread extends Thread {

	private Socket sock;
	private BufferedReader in;
	private PrintWriter out;

	private List<MPCSong> songs;
	private MPC mpc;
	private static ArrayList<Mp3File>playlist;

	/**
	 * Creates an instance of PlaylistThread with the specified settings
	 */
	public PlaylistThread(MPC mpc){
		this.mpc = mpc;
	}

		@Override
		public void run(){

		// Establish socket connection
		try{
			sock = new Socket();
			sock.connect(new InetSocketAddress(mpc.getAddress(), mpc.getPort()), mpc.timeout);
			
			in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			out = new PrintWriter(sock.getOutputStream(), true);

			// Clear version number from buffer
			in.readLine();

			generatePlaylist();
		} catch(IOException e){
			mpc.connectionFailed("Connection failed, check settings");
		}
		
		try{
			sock.close();
			if(in != null) in.close();
			if(out != null) out.close();
		} catch(Exception e){}
	}

	/**
	 */
	
	private void generatePlaylist() throws IOException{

		out.println("playlistinfo");


		playlist=new ArrayList<Mp3File>();
		String response;
		ArrayList<String> list=new ArrayList<String>();
		while(((response = in.readLine()) != null)){
			if (response.startsWith("OK"))break;
			list.add(response);
			if (response.startsWith("Id:")){
				getPlaylist().add(new Mp3File(list));
			}
		}
		int i=0;
		for (Mp3File file:getPlaylist()){
			i++;
			Log.v("samba",""+i+"-"+file.getTitle()+"("+file.getTimeNice()+")");
		}

	}

	public static ArrayList<Mp3File> getPlaylist() {
		return playlist;
	}

}
